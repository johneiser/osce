##
# This module requires Metasploit: https://metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

module MetasploitModule

  CachedSize = 16

  include Msf::Payload::Windows
  include Msf::Payload::Single

  def initialize(info = {})
    super(merge_info(info,
      'Name'		=> 'XOR Encoder/Decoder Stub',
      'Description'	=> 'Encodes and decodes a block of memory using the XOR operation',
      'Author'		=> ['johneiser'],
      'License'		=> MSF_LICENSE,
      'Platform'	=> 'win',
      'Arch'		=> ARCH_X86,
    ))

    register_options([
      OptInt.new("START", [true, "The start of the block of memory to be encoded/decoded"]),
      OptInt.new("END", [true, "The end of the block of memory to be encoded/decoded"]),
      OptInt.new("KEY", [true, "The key with which to XOR encode/decode", 0x0f]),
    ])
  end

  #
  # Construct the payload
  #
  def generate
    mem_start = datastore["START"]
    mem_end = datastore["END"]
    key = datastore["KEY"]
    asm = <<EOS

	mov eax, #{"0x%.8x" % mem_start}
loop_start:
	xor byte [eax], #{"0x%.2x" % key}
	inc eax
	cmp eax, #{"0x%.8x" % mem_end}
	jle.i8 loop_start

EOS
    self.assembly = asm
    super
  end

end
